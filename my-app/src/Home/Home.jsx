import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { modalWindowDeclarations } from "../Modal/modalWindow";



export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            open: false, 
            content: {
               header: "", 
               text: "", 
              actions: [],
              closeButton: true, 
        }};
    }

closeModal = (e) => {
    e.target.closest(".active") ? this.setState({ open: false }) : this.setState({ open: true, status: {} });
}

showModal = (e) => {
    
    let modalData = e.target.getAttribute("data-modal-id");
    let currentItem = modalWindowDeclarations.find(item => item.id === modalData);
    this.setState( {open: true, content: {...currentItem}} )
 }
        
    render(){
        return(
            <>
            <div className="button-wrapper">
            <Button onClick = {this.showModal} data = "1" color="pink" text="Open first modal"/>
            <Button onClick = {this.showModal} data = "2" color="blue" text="Open second modal"/>
            </div>
            
            <Modal 
            className={this.state.open ? "modal-wrapper active" : "modal-wrapper"} 
            close={this.closeModal} 
            header={this.state.content.header}
            text ={this.state.content.text}
            closeButton = {this.state.content.closeButton}
            actions = {this.state.content.actions}
            />
           </>
        );
    }
}