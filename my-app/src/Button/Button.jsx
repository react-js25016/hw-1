import React from "react";
import "./button.scss";

export default class Button extends React.Component{
    
    render(){
        return (
            <button data-modal-id ={this.props.data} onClick={ (e) => this.props.onClick(e) } className="button" style={{backgroundColor: this.props.color}}>{this.props.text}</button>
        )
    }
}